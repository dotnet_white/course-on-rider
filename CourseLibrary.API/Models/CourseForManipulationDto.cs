using System.ComponentModel.DataAnnotations;
using CourseLibrary.API.ValidationAtributes;

namespace CourseLibrary.API.Models
{
    [CourseTitleMustBeDifferentFromDescription(
        ErrorMessage = "Title must be different from description.")]
    public abstract class CourseForManipulationDto
    {
        [Required(ErrorMessage = "You should fill out a title.")]
        [MaxLength(100, ErrorMessage = "the title shouldn't have more than 100 characters.")]
        public string Title { get; set; }
        [MaxLength(500, ErrorMessage = "the description shouldn't have more than 500 characters.")]
        public virtual string Description { get; set; }


    }
}