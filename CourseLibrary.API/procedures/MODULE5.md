## MODULE 5 - FILTERING AND SEARCHING

<hr />

### Filtering resource collections
in AuthorsController we enable filtering through the query

    public ActionResult<IEnumerable<AuthorDto>> GetAuthors([FromQuery]string  mainCategory)
    {
        var authorsFromRepo = _courseLibraryRepository.GetAuthors(mainCategory);
        return Ok(_mapper.Map<IEnumerable<AuthorDto>>(authorsFromRepo));
    }

Now we have to update repository and it's interface with required methods

    public IEnumerable<Author> GetAuthors(string mainCategory)
    {
        if (mainCategory == null)
        {
            return GetAuthors();
        }
    
        mainCategory = mainCategory.Trim();
        return _context.Authors.Where(a => a.MainCategory == mainCategory).ToList();
    }
in Interface add line
    
    IEnumerable<Author> GetAuthors(string mainCategory);

### Searching through resource collection
in AuthorsController we enable searching through the query

    public ActionResult<IEnumerable<AuthorDto>> GetAuthors([FromQuery]string  mainCategory,
            [FromQuery] string searchQuery)
    {
        var authorsFromRepo = _courseLibraryRepository.GetAuthors(mainCategory, searchQuery);
        return Ok(_mapper.Map<IEnumerable<AuthorDto>>(authorsFromRepo));
    }

update Repository method to include search

VIP - IQueryable object lets us deferred execution, so it has better performance torwards the DB, because until iteration happens(".toList()" in  our example) the query to the DB is not executed, previously is only constructed.

    public IEnumerable<Author> GetAuthors(string mainCategory, string searchQuery)
    {
        if (string.IsNullOrWhiteSpace(mainCategory) && string.IsNullOrWhiteSpace(searchQuery))
        {
            return GetAuthors();
        }
        var collection = _context.Authors as IQueryable<Author>;
        if (!string.IsNullOrWhiteSpace(mainCategory))
        {
            mainCategory = mainCategory.Trim();
            collection= collection.Where(a => a.MainCategory == mainCategory);
        }

        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(a => a.MainCategory.Contains(searchQuery) ||
                                               a.FirstName.Contains(searchQuery) ||
                                               a.LastName.Contains(searchQuery));
        }
        
       return collection.ToList();
    }
in Interface update line

    IEnumerable<Author> GetAuthors(string mainCategory, string searchQuery);

### Grouping action parameters
we create a ResourceParameters folder and inside a AuthorsResourceParameters

    public class AuthorsResourceParameters
    {
        public string MainCategory { get; set; }
        public string SearchQuery { get; set; }
    }

update Repository method to include object parameter
    
    public IEnumerable<Author> GetAuthors(AuthorsResourceParameters authorsResourceParameters)
    {
        if (authorsResourceParameters == null)
        {
            throw new ArgumentNullException(nameof(authorsResourceParameters));
        }
    
        if (string.IsNullOrWhiteSpace(authorsResourceParameters.MainCategory) && string.IsNullOrWhiteSpace(authorsResourceParameters.SearchQuery))
        {
            return GetAuthors();
        }
        var collection = _context.Authors as IQueryable<Author>;
        if (!string.IsNullOrWhiteSpace(authorsResourceParameters.MainCategory))
        {
            var mainCategory = authorsResourceParameters.MainCategory.Trim();
            collection= collection.Where(a => a.MainCategory == mainCategory);
        }

        if (!string.IsNullOrWhiteSpace(authorsResourceParameters.SearchQuery))
        {
            var searchQuery = authorsResourceParameters.SearchQuery.Trim();
            collection = collection.Where(a => a.MainCategory.Contains(searchQuery) ||
                                               a.FirstName.Contains(searchQuery) ||
                                               a.LastName.Contains(searchQuery));
        }
        
        return collection.ToList();
    }
update Interface to include object parameter

    IEnumerable<Author> GetAuthors(AuthorsResourceParameters authorsResourceParameters);

update AuthorsController to include new object as parameter

    public ActionResult<IEnumerable<AuthorDto>> GetAuthors([FromQuery]AuthorsResourceParameters authorsResourceParameters)
    {
        var authorsFromRepo = _courseLibraryRepository.GetAuthors(authorsResourceParameters);
        return Ok(_mapper.Map<IEnumerable<AuthorDto>>(authorsFromRepo));
    }