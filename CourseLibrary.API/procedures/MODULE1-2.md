## MODULE 1 And 2 - GETTING STARTED

<hr />


#### first create project netcore WebAPI

#### Create .gitignore file and create git repository
in shell window inside the folders project
    dotnet new gitignore
    git init
    git add .
    git commit -m "first commit, well done!!"

#### Disable browser open and disable https, and change http port to 51044
* in launchSettings.json

#### disable https redirection
* in Startup.cs

#### copying files for starting point of the backend
[Starter Files](https://github.com/KevinDockx/BuildingRESTfulAPIAspNetCore3/tree/master/Starter%20files "With a Title").
* DbContext
* Entities
* Repositories
* edit Program.cs
* edit Startup.cs

#### Update starter.cs file to use SqlLite DB instead of default
    options.UseSqlite("Data Source=localdatabase.db"));
    // comment next line
    //  options.UseSqlServer(Configuration.GetConnectionString("MyDbConnection")));

#### Install entityFramework
on nuget package manager
* microsoft.EntityframeworkCore
* microsoft.EntityframeworkCore.sqlserver
* microsoft.EntityframeworkCore.tools
* microsoft.EntityframeworkCore.sqlLite

#### add migrations
on terminal

    dotnet ef migrations add InitialMigration