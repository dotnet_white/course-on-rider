## MODULE 3 - OUTER FACING CONTRACT

<hr />

#### Add Controller AuthorsController.cs and GetAuthors endpoint
in controller folder

    [ApiController]
    [Route("/api/authors")]
    public class AuthorController : ControllerBase //Controller can be extended from this, but includes views support, not needed in APIs
    {
    private readonly ICourseLibraryRepository _courseLibraryRepository;

        public AuthorController(ICourseLibraryRepository courseLibraryRepository)
        {
            _courseLibraryRepository = courseLibraryRepository ??
                                       throw new ArgumentNullException(nameof(courseLibraryRepository));
        }

        [HttpGet]
        public IActionResult GetAuthors()
        {
            var authorsFromRepo = _courseLibraryRepository.GetAuthors();

            return new JsonResult(authorsFromRepo);
        }
    }

#### Add GetAuthor Endpoint

    [HttpGet("{authorId}")]
    public IActionResult GetAuthor(Guid authorId)
    {
        var authorFromRepo = _courseLibraryRepository.GetAuthor(authorId);
        if (authorFromRepo == null)
        {
            return NotFound();
        }
        return new JsonResult(authorFromRepo);
    }

#### Subtitute new JsonResult to Ok
Because in this way we can send other formats than json, like XML

    //in example...
    return Ok(authorFromRepo);

#### Support to XML format response
handling unsopported media types on configure service in Startup.cs class

    services.AddControllers(setupAction =>
    {
       setupAction.ReturnHttpNotAcceptable = true;
    });

support adtional formats in this case XML

    services.AddControllers(setupAction =>
    {
       setupAction.ReturnHttpNotAcceptable = true;
    }).AddXmlDataContractSerializerFormatters();



![GitHub Logo](cat.png)
