## MODULE 8 - UPDATING RESOURCES

<hr />

### Updating a Resource
create put endpoint UpdateCourseForAuthor in CoursesController

    [HttpPut("{courseId}")]
    public ActionResult UpdateCourseForAuthor(Guid authorId, Guid courseId, CourseForUpdateDto course)
    {
        if (!_courseLibraryRepository.AuthorExists(authorId))
        {
        return NotFound();
        }
        var courseForAuthorFromRepo = _courseLibraryRepository.GetCourse(authorId, courseId);
        if (courseForAuthorFromRepo == null)
        {
            return NotFound();
        }
        // map the entity to a CourseForUpdateDto
        // apply the updated field values to that dto
        // map the CourseforUpdateDto back to an entity
        _mapper.Map(course, courseForAuthorFromRepo);
        _courseLibraryRepository.UpdateCourse(courseForAuthorFromRepo);
        _courseLibraryRepository.Save();
        return NoContent();
    }

Create DTO CourseForupdateDto

    public class CourseForUpdateDto
    {
    public string Title { get; set; }
    public string Description { get; set; }
    }

a mapping has to be added in the course profile

    CreateMap<Models.CourseForUpdateDto, Entities.Course>();
    
### Validating input when updating Resource
most same validations as in CourseForCreationDto are needed,
So in order to minimize Duplicate code we're going to create a base class that is going to be
inherit by the CourseForCreationDto and CourseForUpdateDto

we Create a CourseForManipulationDto abstract class

    [CourseTitleMustBeDifferentFromDescription(
        ErrorMessage = "Title must be different from description.")]
    public abstract class CourseForManipulationDto
    {
        [Required(ErrorMessage = "You should fill out a title.")]
        [MaxLength(100, ErrorMessage = "the title shouldn't have more than 100 characters.")]
        public string Title { get; set; }
        [MaxLength(500, ErrorMessage = "the description shouldn't have more than 500 characters.")]
        public virtual string Description { get; set; }
    }
VIP - Note that field description has "virtual" modifier in order to be overriding when needed(if putted abstract will always be needed the override)

so we rewrite CourseForUpdateDto
    
    public class CourseForUpdateDto : CourseForManipulationDto
    {
        [Required(ErrorMessage = "You should fill out a Description.")]
        public override string Description { get => base.Description; set => base.Description= value; }
    }
and rewrite CourseForCreationDto

    public class CourseForCreationDto  : CourseForManipulationDto
    {
    }

VIP - and remenber to update the classes where CourseForCreationDto used and needs to implement now CourseForManipulationDto,
like CourseTitleMustBeDifferentFromDescriptionAttribute

### upserting with put
Upserting is the action of entering a new resource with put when it is not present in the repository
Let's Update UpdateCourseForAuthor...

    [HttpPut("{courseId}")]
    public IActionResult UpdateCourseForAuthor(Guid authorId, Guid courseId, CourseForUpdateDto course)
    {
        if (!_courseLibraryRepository.AuthorExists(authorId))
        {
            return NotFound();
        }
        var courseForAuthorFromRepo = _courseLibraryRepository.GetCourse(authorId, courseId);
        if (courseForAuthorFromRepo == null)
        {
            var courseToAdd = _mapper.Map<Course>(course);
            courseToAdd.Id = courseId;
            _courseLibraryRepository.AddCourse(authorId, courseToAdd);
            _courseLibraryRepository.Save();
            var courseToReturn = _mapper.Map<CourseDto>(courseToAdd);
            return CreatedAtRoute("GetCourseforAuthor",
                new {authorId = authorId, courseId = courseToReturn.Id},
                courseToReturn);
        }
        _mapper.Map(course, courseForAuthorFromRepo); 
        _courseLibraryRepository.UpdateCourse(courseForAuthorFromRepo);
        _courseLibraryRepository.Save();
        return NoContent();
    }

### Partially Updating a Resource
Let's Create an endpoint in coursesController to support Patch

    [HttpPatch("{courseId}")]
    public ActionResult PartiallyUpdateCourseForAuthor(Guid authorId,
        Guid courseId,
        JsonPatchDocument<CourseForUpdateDto> patchDocument)
    {
        if (!_courseLibraryRepository.AuthorExists(authorId))
        {
            return NotFound();
        }
        var courseForAuthorFromRepo = _courseLibraryRepository.GetCourse(authorId, courseId);
        if (courseForAuthorFromRepo == null)
        {
            return NotFound();
        }
        var courseToPatch = _mapper.Map<CourseForUpdateDto>(courseForAuthorFromRepo);
        // add validation
        patchDocument.ApplyTo(courseToPatch);
        _mapper.Map(courseToPatch, courseForAuthorFromRepo);
        _courseLibraryRepository.UpdateCourse(courseForAuthorFromRepo);
        _courseLibraryRepository.Save();
        return NoContent();``
    }

on nuget package manager
* microsoft.AspNetCore.JsonPatch
* microsoft.AspNetCore.Mvc.NewtonsoftJson

Lets Create a Profile Map for the use in this case
    
    CreateMap<Entities.Course, Models.CourseForUpdateDto>();

Add some lines in Startup in configure services to full support to json in patches

    services.AddControllers(setupAction =>
    {
        setupAction.ReturnHttpNotAcceptable = true;
    }).AddNewtonsoftJson(setupAction =>
    {
        setupAction.SerializerSettings.ContractResolver =
            new CamelCasePropertyNamesContractResolver();
    }).AddXmlDataContractSerializerFormatters()