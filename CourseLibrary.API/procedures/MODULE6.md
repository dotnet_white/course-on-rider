## MODULE 6 - CREATING RESOURCES

<hr />

### Creating a Resource
first create CreateAuthor in AuthorsController

    [HttpPost]
    public ActionResult<AuthorDto> CreateAuthor(AuthorForCreationDto author)
    {
        var authorEntity = _mapper.Map<Entities.Author>(author);
        _courseLibraryRepository.AddAuthor(authorEntity);
        _courseLibraryRepository.Save();
        var authorToReturn = _mapper.Map<AuthorDto>(authorEntity);
        return CreatedAtRoute("GetAuthor",
                    new {authorId = authorToReturn.Id}, authorToReturn);
    }

let's now create AuthorForCreationDto.cs

    public class AuthorForCreationDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTimeOffset DateOfBirth { get; set; }          

        public string MainCategory { get; set; }

    }

Let's create a profile mapper for author creation Dto

    CreateMap<Models.AuthorForCreationDto, Entities.Author>();

Add label to getAuthor

    [HttpGet("{authorId}", Name= "GetAuthor")]
    public ActionResult<AuthorDto> GetAuthor(Guid authorId)

### Creating a child Resource
now we are going to create a course for an author.

first create CreateCourseForAuthor in CoursesController

    [HttpPost]
    public ActionResult<CourseDto> CreateCourseForAuthor(Guid authorId, CourseForCreationDto course)
    {
        if (!_courseLibraryRepository.AuthorExists(authorId))
        {
            return NotFound();
        }
        var courseEntity = _mapper.Map<Entities.Course>(course);
        _courseLibraryRepository.AddCourse(authorId,courseEntity);
        _courseLibraryRepository.Save();
        var courseToReturn = _mapper.Map<CourseDto>(courseEntity);
        return CreatedAtRoute("GetCourseforAuthor",
            new {authorId = authorId, courseId = courseToReturn.Id},
            courseToReturn);

    }
let's now create CourseForCreationDto.cs

    public class CourseForCreationDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }

Let's create a profile mapper for Course creation Dto

    CreateMap<Models.CourseForCreationDto, Entities.Course>();

Add label to GetCourseForAuthor
    
    [HttpGet("{courseId}", Name = "GetCourseforAuthor")]
    public ActionResult<CourseDto> GetCourseForAuthor(Guid authorId, Guid courseId)

### Creating Child together with parent resources
add a collection of childs in parent creation DTO
In our case create a collection of courses inside AuthorForCreationDto

    public ICollection<CourseForCreationDto> Courses { get; set; }
        = new List<CourseForCreationDto>();

### Creating a collection of resources
this is to pass collections of items to the repository
In this case we're introducing a group of authors to the DB, 
through Creating a new controller called AuthorCollectionsController
    
    [ApiController]
    [Route("api/authorcollections")]
    public class AuthorCollectionsController: ControllerBase
    {
        private readonly ICourseLibraryRepository _courseLibraryRepository;
        private readonly IMapper _mapper;
        
        public AuthorCollectionsController(ICourseLibraryRepository courseLibraryRepository, IMapper mapper)
        {
            _courseLibraryRepository = courseLibraryRepository ??
                            throw new ArgumentNullException(nameof(courseLibraryRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpPost]
        public ActionResult<IEnumerable<AuthorDto>> CreateAuthorCollection(
                IEnumerable<AuthorForCreationDto> authorCollection)
        {
            var authorEntities = _mapper.Map<IEnumerable<Entities.Author>>(authorCollection);
            foreach (var author in authorEntities)
            {
                _courseLibraryRepository.AddAuthor(author);
            }
            _courseLibraryRepository.Save();
            return Ok();
        }
        
    }

VIP - returns Ok but this is not correct. must be corrected in future modifications.

### working with array keys and composite keys
We're going to create a GET of a collection of authors by passing an array of Id(are Guids). 
But there in no implicit binding to such part of route

first we create Get end point for getting a collection of authors

    [HttpGet("({ids})", Name = "GetAuthorCollection")]
    public IActionResult GetAuthorCollection(
    [FromRoute]
    [ModelBinder(BinderType  =  typeof(ArrayModelBinder))] IEnumerable<Guid> ids)
    {
    if (ids == null)
    {
    return BadRequest();
    }
        var authorEntities = _courseLibraryRepository.GetAuthors(ids);
        if (ids.Count() != authorEntities.Count())
        {
            return NotFound();
        }
    
        var authorsToReturn = _mapper.Map<IEnumerable<AuthorDto>>(authorEntities);
        return Ok(authorsToReturn);

    }

Lets preparate a custom model binder in order to get list of Ids in a propper format
Create a Class ArrayModelBinder in Helpers folder

    public class ArrayModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            //Our Binder works only on enumerable types
            if (!bindingContext.ModelMetadata.IsEnumerableType)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return Task.CompletedTask;
            }
            
            //Get the inputted value through the value provider
            var value = bindingContext.ValueProvider.
                GetValue(bindingContext.ModelName).ToString();
            
            //If that value is null or whitespace, we return null
            if (string.IsNullOrWhiteSpace(value))
            {
                bindingContext.Result = ModelBindingResult.Success(null);
                return Task.CompletedTask;
            }
            // the value isn't null or whitespace,
            // and the type of the model is enumerable.
            // Get the enumerable's type, and a converter
            var elementType = bindingContext.ModelType.GetTypeInfo().GenericTypeArguments[0];
            var converter = TypeDescriptor.GetConverter(elementType);
            
            //convert each item in the value list to the enumerable type
            var values = value.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => converter.ConvertFromString(x.Trim()))
                .ToArray();
            
            //Create an array of the type, and set it as the Model value
            var typedValues = Array.CreateInstance(elementType, values.Length);
            values.CopyTo(typedValues, 0);
            bindingContext.Model = typedValues;
            
            //return a successful result, passing in the Model
            bindingContext.Result = ModelBindingResult.Success(bindingContext.Model);
            return Task.CompletedTask;

            //throw new System.NotImplementedException();
        }
    }
Now we can update the CreateAuthorCollection Endpoint to return correctly
    
    //Remove return Ok(); and places the nexts lines instead!
    var authorCollectionToReturn = _mapper.Map<IEnumerable<AuthorDto>>(authorEntities);
    var idsAsString = string.Join(",", authorCollectionToReturn.Select(a => a.Id));
    return CreatedAtRoute("GetAuthorCollection",
    new {ids = idsAsString},
    authorCollectionToReturn);
    
### Supporting Options EndPoint
this is just an endpoint that returns a list of the verbs that can be used in a Route

lets create endpoint on GetAuthorsOptions

    [HttpOptions]
    public IActionResult GetAuthorsOptions()
    {
        Response.Headers.Add("Allow", "GET, OPTIONS, POST");
        return Ok();
    }

### SUPPORTING ADITIONAL INPUT FORMATS
in Startup class in configureService method the XML Serializer accepts output and input formats,
so in this way our application can support Json and Xml , in input and also output. 