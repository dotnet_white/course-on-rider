## MODULE 4 - GETTING REOURCES

<hr />

#### authorDTO Creation
create class for authorDTO in Models Folder

    namespace CourseLibrary.API.Models
    {
        public class AuthorDto
        {
            public Guid Id { get; set; }
    
            public string Name { get; set; }
    
            public int Age { get; set; }          
    
            public string MainCategory { get; set; }
        }
    }

change return value from endpoints to DTO in AuthorsController

    var authors = new List<AuthorDto>();

    foreach (var author in authorsFromRepo)
    {
        authors.Add(new AuthorDto()
        {
            Id=author.Id,
            Name = $"{author.FirstName} {author.LastName}",
            MainCategory = author.MainCategory,
            Age = author.DateOfBirth.GetCurrentAge()
        });
    }
    return Ok(authors);
create Helpers folder with class DateTimeOffsetextensions to implement GetCurrentAge method

    public static class DateTimeOffsetExtensions
    {
        public static int GetCurrentAge(this DateTimeOffset dateTimeOffset)
        {
            var currentDate = DateTime.Now;
            int age = currentDate.Year - dateTimeOffset.Year;
            if (currentDate < dateTimeOffset.AddYears(age))
            {
                age--;
            }

            return age;
        }
    }

#### improving action Result
    public ActionResult<IEnumerable<AuthorDto>> GetAuthors()
    {
    // method code!!! 
    }

#### AuthoMapper
on nuget package manager install
* microsoft.EntityframeworkCore

configure service on Startup.cs in configureServices method
    
    services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

Create Profiles folder with class AuthorsProfile to implement Authors profiles

    public class AuthorsProfile : Profile
    {
        public AuthorsProfile()
        {
            CreateMap<Entities.Author, Models.AuthorDto>()
                .ForMember(
                    dest => dest.Name,
                    opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(
                    dest => dest.Age,
                    opt => opt.MapFrom(src => src.DateOfBirth.GetCurrentAge()));
        }
    }

in authorsController inject Mapper in constructor

    private readonly ICourseLibraryRepository _courseLibraryRepository;
    private readonly IMapper _mapper;
    public AuthorController(ICourseLibraryRepository courseLibraryRepository, IMapper mapper)
    {
        _courseLibraryRepository = courseLibraryRepository ??
            throw new ArgumentNullException(nameof(courseLibraryRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

Also in AuthorsController modify GetAuthors Endpoint to use Mapper instead of foreach
    
    public ActionResult<IEnumerable<AuthorDto>> GetAuthors()
    {
        var authorsFromRepo = _courseLibraryRepository.GetAuthors();
        return Ok(_mapper.Map<IEnumerable<AuthorDto>>(authorsFromRepo));
    }

Lets do same to GetAuthor EndPoint

    public ActionResult<AuthorDto> GetAuthor(Guid authorId)
    {
        var authorFromRepo = _courseLibraryRepository.GetAuthor(authorId);
        if (authorFromRepo == null)
        {
            return NotFound();
        }
        return Ok(_mapper.Map<AuthorDto>(authorFromRepo));
    }
Add CoursesController   

    [ApiController]
    [Route("api/authors/{authorId}/courses")]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseLibraryRepository _courseLibraryRepository;
        private readonly IMapper _mapper;
        public CoursesController(ICourseLibraryRepository courseLibraryRepository, IMapper mapper)
        {
            _courseLibraryRepository = courseLibraryRepository ??
                    throw new ArgumentNullException(nameof(courseLibraryRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
    }

add CourseDto

    public class CourseDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid AuthorId { get; set; }
    }

add courseProfile for autoMapper

    public class CoursesProfile : Profile
    {
        public CoursesProfile()
        {
            CreateMap<Entities.Course, Models.CourseDto>();
        }
    }

Once done this lets back to the CoursesController. First lets do GetCoursesForAuthor
    
    [HttpGet()]
    public ActionResult<IEnumerable<CourseDto>> GetCoursesForAuthor(Guid authorId)
    {
        if (!_courseLibraryRepository.AuthorExists(authorId))
        {
            return NotFound();
        }
        var coursesForAuthorFromRepo = _courseLibraryRepository.GetCourses(authorId);
        return Ok(_mapper.Map<IEnumerable<CourseDto>>(coursesForAuthorFromRepo));
    }

Let's do GetCourseForAuthor Endpoint

    [HttpGet("{courseId}")]
    public ActionResult<CourseDto> GetCourseForAuthor(Guid authorId, Guid courseId)
    {
        if (!_courseLibraryRepository.AuthorExists(authorId))
        {
            return NotFound();
        }
        var courseForAuthorFromRepo = _courseLibraryRepository.GetCourse(authorId, courseId);
        if (courseForAuthorFromRepo == null)
        {
            return NotFound();
        }
        return Ok(_mapper.Map<CourseDto>(courseForAuthorFromRepo));
    }

#### Handling Faults
in startup.cs lets edit configure method to handle unexpected errors. Only going to be in use in production
    
    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    } else {
        app.UseExceptionHandler(appBuilder =>
        {
            appBuilder.Run(async context =>
            {
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync("An unexpected fault happened. Try again later.");
            });
        });
    }

#### HEAD atribute
add [HttpHead] label on endpoints to get the same response but without body

    [HttpGet()]
    [HttpHead]
    public ActionResult<IEnumerable<AuthorDto>> GetAuthors() {//some code...}
