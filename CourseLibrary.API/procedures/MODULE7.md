## MODULE 7 - Validation Data and reporting Validation errors

<hr />

### Validation input with data annotations
In this case we validate input constraints with annotations in couseCreationDto

    public class CourseForCreationDto
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(1500)]
        public string Description { get; set; }
    }

### class-Level input validation with IvalidatableObject
increment the CourseForCreationDto class implementing an interface

    public class CourseForCreationDto : IValidatableObject
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(1500)]
        public string Description { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Title == Description)
            {
                yield return new ValidationResult(
                    "The provided description should be different from the title.",
                    new[] {"CourseForCreationDto"});
            }
        }
    }
### class-Level input validation with a custom Attribute
lets create a folder validationatributes and a class CourseTitleMustBeDifferentFromDescriptionAttribute and there we define the custom atribute

    public class CourseTitleMustBeDifferentFromDescriptionAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            var course = (CourseForCreationDto) validationContext.ObjectInstance;
            if (course.Title == course.Description)
            {
                return new ValidationResult(
                    "the provided description should be different from the title.",
                    new[] {nameof(CourseForCreationDto)});
            }
            return ValidationResult.Success;
        }
    }
Now we update CourseForCreationDto in order to support this new rule. 
This new implementation substitutes in this example iValidatableObject Implementation. But they can live together
custom attributes is executed before Validate Method when both are present.

    [CourseTitleMustBeDifferentFromDescription]
    public class CourseForCreationDto // : IValidatableObject
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(1500)]
        public string Description { get; set; }
        
        /*public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
            {
                if (Title == Description)
                {
                    yield return new ValidationResult(
                        "The provided description should be different from the title.",
                        new[] {"CourseForCreationDto"});
                }
            }*/
    }
 ### Customizing error messages
Just applying a errorMessage in the annotation of the validation

    [CourseTitleMustBeDifferentFromDescription(
    ErrorMessage = "Title must be different from description.")]
    public class CourseForCreationDto// : IValidatableObject
    {
    [Required(ErrorMessage = "You should fill out a title.")]
    [MaxLength(100, ErrorMessage = "the title shouldn't have more than 100 characters.")]
    public string Title { get; set; }
    [MaxLength(500, ErrorMessage = "the description shouldn't have more than 500 characters.")]

In case custom attribute we have to put ErrorMessage instead of the error message

    return new ValidationResult(
                    ErrorMessage,
                    new[] {nameof(CourseForCreationDto)});

### Customizing validation error responses
Adding some code to handle this on Startup.cs in ConfigureServices Method

    .ConfigureApiBehaviorOptions(setupAction =>
    {
        setupAction.InvalidModelStateResponseFactory = context =>
        {
            // create a problem details object
            var problemDetailsFactory = context.HttpContext.RequestServices
                .GetRequiredService<ProblemDetailsFactory>();
            var problemDetails = problemDetailsFactory.CreateValidationProblemDetails(
                context.HttpContext,
                context.ModelState);
            
            // add additional info not added by default
            problemDetails.Detail = "See the errors field for details.";
            problemDetails.Instance = context.HttpContext.Request.Path;
            
            //find out which status code to use
            var actionExecutingContext =
                context as Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext;
            
            // if there are modelstate errors & all arguments were correctly
            // found/parsed we're dealing with validation errors
            if ((context.ModelState.ErrorCount > 0) &&
                (actionExecutingContext?.ActionArguments.Count ==
                 context.ActionDescriptor.Parameters.Count))
            {
                problemDetails.Type = "https://courselibrary.com/modelvalidationproblem";
                problemDetails.Status = StatusCodes.Status422UnprocessableEntity;
                problemDetails.Title = "One or more validation errors ocurred.";
                return new UnprocessableEntityObjectResult(problemDetails)
                {
                    ContentTypes = {"application/problem+json"}
                };
            };
            // if one of the arguments wasn't correctly found / couldn't be parsed
            // we're dealing with null/unparseable input
            problemDetails.Status = StatusCodes.Status400BadRequest;
            problemDetails.Title = "One or more errors on input occurred.";
            return new BadRequestObjectResult(problemDetails)
            {
                ContentTypes = {"application/problem+json"}
            };
        };
    });
